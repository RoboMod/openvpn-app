#!/usr/bin/env node

/* global describe, before, after, xit, it */
'use strict'

require('chromedriver')

const execSync = require('child_process').execSync
const expect = require('expect.js')
const net = require('net')
const path = require('path')

const selenium = require('selenium-webdriver')
const chrome = require('selenium-webdriver/chrome')

const by = selenium.By
const until = selenium.until

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
selenium.promise.USE_PROMISE_MANAGER = false

const VPN_TCP_PORT = 7333

const DEVICE_NAME = 'test_DeV1ce-3'

if (!process.env.CLOUDRON_USERNAME || !process.env.CLOUDRON_PASSWORD) {
  console.log('CLOUDRON_USERNAME and CLOUDRON_PASSWORD env vars need to be set')
  process.exit(1)
}

describe('Application life cycle test', function () {
  this.timeout(0)

  let browser
  const username = process.env.CLOUDRON_USERNAME
  const password = process.env.CLOUDRON_PASSWORD

  before(function () {
    browser = new selenium.Builder()
      .forBrowser('chrome')
      .setChromeOptions(new chrome.Options().addArguments(['no-sandbox' /*, 'headless' */]))
      .build()
  })

  after(function () {
    browser.quit()
  })

  const LOCATION = 'test'
  const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000
  let app

  function waitForElement (elem) {
    return browser.wait(until.elementLocated(elem), TEST_TIMEOUT)
      .then(() => browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT))
  }

  function getAppInfo () {
    const inspect = JSON.parse(execSync('cloudron inspect'))
    app = inspect.apps.filter(a => a.location === LOCATION || a.location === LOCATION + '2')[0]
    expect(app).to.be.an('object')
  }

  function login () {
    console.log('Logging in...')
    return browser.manage().deleteAllCookies()
      .then(() => console.log('Cookies deleted'))
      .then(() => browser.get('https://' + app.fqdn))
      .then(() => console.log('Page loaded'))
      .then(() => browser.wait(until.elementLocated(by.id('loginUsername')), TEST_TIMEOUT))
      .then(() => console.log('Login page loaded'))
      .then(() => browser.findElement(by.id('loginUsername')).sendKeys(username))
      .then(() => browser.findElement(by.id('loginPassword')).sendKeys(password))
      .then(() => browser.findElement(by.id('loginButton')).click())
      .then(() => console.log('Login clicked'))
      .then(() => browser.wait(until.elementLocated(by.id('logoutButton')), TEST_TIMEOUT))
      .then(() => console.log('Logged in'))
  }

  function logout () {
    console.log('Logging out...')

    return waitForElement(by.id('logoutButton'))
      .then(() => browser.findElement(by.id('logoutButton')).click())
      .then(() => waitForElement(by.id('loginButton')))
      .then(() => console.log('Logged out'))
  }

  function createDevice (name) {
    console.log('Creating device...')

    return waitForElement(by.id('newDeviceButton'))
      .then(() => browser.findElement(by.id('newDeviceButton')).click())
      .then(() => console.log('Open Dialog'))
      .then(() => waitForElement(by.xpath('/html/body/div[2]/div/div[2]/div/div[1]/input')))
      .then(() => browser.findElement(by.xpath('/html/body/div[2]/div/div[2]/div/div[1]/input')).sendKeys(name))
      .then(() => browser.findElement(by.xpath('/html/body/div[2]/div/div[3]/button[2]')).click())
      .then(() => console.log('Device created'))
      .then(() => browser.sleep(2000));
  }

  function checkDeviceExists (name) {
    return browser.wait(until.elementLocated(by.xpath(`//div[text()="${name}"]`)), TEST_TIMEOUT)
  }

  function checkVpn (done) {
    const client = net.connect({host: app.fqdn, port: VPN_TCP_PORT}, () => {
      client.end()
      done()
    })
    client.on('error', done)
  }

  xit('build app', function () {
    execSync('cloudron build', {cwd: path.resolve(__dirname, '..'), stdio: 'inherit'})
  })

  it('install app', function () {
    execSync('cloudron install --new --wait --location ' + LOCATION + ' -p VPN_TCP_PORT=' + VPN_TCP_PORT, {
      cwd: path.resolve(__dirname, '..'),
      stdio: 'inherit'
    })
  })

  it('can get app information', getAppInfo)
  it('can login', login)
  it('can create device', () => createDevice(DEVICE_NAME))
  it('device exists', () => checkDeviceExists(DEVICE_NAME))
  it('check vpn', checkVpn)
  it('can logout', logout)

  it('can restart app', function () {
    execSync('cloudron restart --wait --app ' + app.id)
  })

  it('can login', login)
  it('device exists', () => checkDeviceExists(DEVICE_NAME))
  it('check vpn', checkVpn)
  it('can logout', logout)

  it('backup app', function () {
    execSync('cloudron backup create --app ' + app.id, {cwd: path.resolve(__dirname, '..'), stdio: 'inherit'})
  })

  it('restore app', function () {
    execSync('cloudron restore --app ' + app.id, {cwd: path.resolve(__dirname, '..'), stdio: 'inherit'})
  })

  it('can login', login)
  it('device exists', () => checkDeviceExists(DEVICE_NAME))
  it('check vpn', checkVpn)
  it('can logout', logout)

  it('move to different location', function () {
    execSync('cloudron configure --wait --location ' + LOCATION + '2 --app ' + app.id, {
      cwd: path.resolve(__dirname, '..'),
      stdio: 'inherit'
    })
  })
  it('can get new app information', getAppInfo)

  it('can login', login)
  it('device exists', () => checkDeviceExists(DEVICE_NAME))
  it('check vpn', checkVpn)
  it('can logout', logout)

  it('uninstall app', function () {
    execSync('cloudron uninstall --app ' + app.id, {cwd: path.resolve(__dirname, '..'), stdio: 'inherit'})
  })

  // test update (this test will only work after app is published)
  it('can install app', function () {
    execSync('cloudron install --new --wait --appstore-id io.cloudron.openvpn --location ' + LOCATION + ' -p VPN_TCP_PORT=' + VPN_TCP_PORT, {
      cwd: path.resolve(__dirname, '..'),
      stdio: 'inherit'
    })
  })

  it('can get app information', getAppInfo)
  it('can login', login)
  it('can create device', () => createDevice(DEVICE_NAME))
  it('device exists', () => checkDeviceExists(DEVICE_NAME))
  it('can logout', logout)

  it('can update', function () {
    execSync('cloudron install --wait --app ' + app.id, {cwd: path.resolve(__dirname, '..'), stdio: 'inherit'})
  })

  it('can login', login)
  it('device exists', () => checkDeviceExists(DEVICE_NAME))
  it('check vpn', checkVpn)
  it('can logout', logout)

  it('uninstall app', function () {
    execSync('cloudron uninstall --app ' + app.id, {cwd: path.resolve(__dirname, '..'), stdio: 'inherit'})
  })
})
