FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

EXPOSE 8000

RUN mkdir -p /app/code
WORKDIR /app/code

## Installing OpenVPN, key-management tool, and iptables
RUN apt-get update -y && apt-get install -y openvpn=2.4.4-2ubuntu1.3 easy-rsa iptables dnsmasq && rm -rf /var/cache/apt /var/lib/apt/lists

## Creating easyRSA dir & configuring
RUN make-cadir /app/code/easyrsa
RUN chmod +rx /app/code/easyrsa
RUN sed -e 's,^RANDFILE\s*= \$ENV::HOME/\.rnd$,RANDFILE                = /tmp/.rnd,' -i /app/code/easyrsa/openssl-1.0.0.cnf
ADD easyrsa-vars.sh /app/code/easyrsa/
# Workaround quantum permissions bug
RUN chown -R cloudron:cloudron /app/code/easyrsa

## Installing web-admin interface & packaging scripts
ADD package.json package-lock.json /app/code/
ADD src /app/code/src
ADD frontend /app/code/frontend
ADD hooks /app/code/hooks
ADD start.sh server.js openvpn.conf.template regen-crl.sh /app/code/

# Somehow postinstall is not run automatically when building docker image
RUN npm install --production && npm run postinstall

## Setting up TUN device
RUN mknod /app/code/net-tun c 10 200

## Supervisor
ADD supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
RUN sed -e 's,^chmod=.*$,chmod=0760\nchown=cloudron:cloudron,' -i /etc/supervisor/supervisord.conf

CMD [ "/app/code/start.sh" ]
