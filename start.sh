#!/bin/bash

set -eu

export NODE_ENV=production

# Creating a secret for web sessions
if [ ! -f /app/data/session.secret ]; then
    echo "==> Generating session secret"
    dd if=/dev/urandom bs=256 count=1 2>/dev/null | base64 > /app/data/session.secret
fi

# Generate random management token for admin api
dd if=/dev/urandom bs=256 count=1 2>/dev/null | base64 > /run/admin-token

# The first time this is run, initialize OpenVPN keys
if [ ! -d /app/data/keys ]; then
    echo "==> Init OpenVPN CA"
    source /app/code/easyrsa/easyrsa-vars.sh
    /app/code/easyrsa/clean-all
    /app/code/easyrsa/pkitool --initca              # ca.key and ca.crt
    openvpn --genkey --secret /app/data/keys/ta.key # OpenVPN static key
    /app/code/easyrsa/build-dh
    /app/code/easyrsa/pkitool --server cloudron     # server key
fi

# initializing / regenerating CRL file
echo "==> Creating CRL"
/app/code/regen-crl.sh

# Writing OpenVPN config
echo "==> Generating OpenVPN config"
[[ ! -f /app/data/openvpn.conf ]] && cp /app/code/openvpn.conf.template /app/data/openvpn.conf

sed -e "s/^port .*/port ${VPN_TCP_PORT:-}/" \
    -e "s/^push \"dhcp-option DOMAIN .*\"/push \"dhcp-option DOMAIN ${CLOUDRON_APP_DOMAIN}\"/" \
    -i /app/data/openvpn.conf

# Add iptables rules for NATing VPN traffic
network=$(cat /app/data/openvpn.conf | sed -ne 's/^server \(.*\) .*$/\1/p')
echo "==> Configuring nat rules for ${network}"
iptables -t nat -A POSTROUTING -s $network/24 -o eth0 -j MASQUERADE

# Clear all hosts on startup
mkdir -p /run/dnsmasq/hosts

# Fix permissions
echo "==> Fixing permissions"
chown -R cloudron:cloudron /app/data /tmp /run

echo "Starting OpenVPN"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i OpenVPN
