/* global superagent, $, Vue */

(function () {
  'use strict'

  window.app = new Vue({
    el: '#app',
    data: {
      busy: true,
      entries: [],
      user: null,
      loginForm: {username: '', password: ''},
      connectedClients: {}
    },
    computed: {
      connectedClientCount () {
        return this.entries.filter(e => e.connected).length
      }
    },
    created () {
      this.welcome()
    },
    methods: {
      logout () {
        superagent.get('/api/logout')
          .end((error, result) => {
            if (error) return console.error(error)

            this.user = null
            this.connectedClients = {}
            this.entries = []
          })
      },
      login (username, password) {
        this.busy = true

        superagent.post('/api/login', {username: username, password: password})
          .end((error, result) => {
            this.busy = false

            this.loginForm.password = ''

            if (result && result.statusCode === 401) {
              this.$nextTick(() => this.$refs.passwordInput.focus())
              return
            }
            if (error) return console.error(error)

            this.loginForm.username = ''
            this.user = result.body.user

            this.refresh()
          })
      },
      welcome () {
        this.busy = true

        superagent.get('/api/profile')
          .end((error, result) => {
            this.busy = false

            if (result && result.statusCode === 401) return this.user = null
            if (error) return console.error(error)

            this.user = result.body.user

            this.refresh()
          })
      },
      refresh () {
        this.busy = true

        superagent.get('/api/list/')
          .end((error, result) => {
            this.busy = false

            if (result && result.statusCode === 401) return this.logout()
            if (error) {
              this.$message.error(error)
              console.error(error)
              return
            }

            this.entries = result.body.entries
            if (result.body.connectedClients) this.connectedClients = result.body.connectedClients
          })
      },
      createKeyAsk () {
        this.$prompt('', 'New Device Name', {
          confirmButtonText: 'Create',
          cancelButtonText: 'Cancel',
          confirmButtonLoading: false,
          inputPattern: /^[a-z0-9-_]+$/i,
          inputErrorMessage: 'Invalid Device Name',
          beforeClose: (action, instance, done) => {
            if (action === 'confirm') {
              instance.confirmButtonLoading = true
              instance.confirmButtonText = 'Creating...'

              const onError = error => {
                console.error(error)

                instance.confirmButtonLoading = false
                instance.confirmButtonText = 'Create'

                this.$message.error(error)
              }

              superagent.put('/api/key/' + instance.inputValue)
                .end((error, result) => {
                  if (result && result.statusCode === 401) return this.logout()
                  if (result && result.statusCode === 409) return onError('Invalid device name')
                  if (result && result.statusCode !== 201) return onError('Something went wrong')
                  if (error) return onError(error)

                  this.refresh()

                  done()
                })
            } else {
              done()
            }
          }
        }).then(action => {}) // handle the promise rejection on dismiss
      },
      revokeKeyAsk (name) {
        this.$msgbox({
          title: 'Really revoke the key for ' + name + '?',
          message: '<div role="alert" class="el-alert el-alert--error"><i class="el-alert__icon el-icon-warning"></i><div class="el-alert__content"><span class="el-alert__title">You won\'t be able to undo this action!</span></div></div><br/><p>To confirm, please enter the device name <b>' + name + '</b></p>',
          dangerouslyUseHTMLString: true,
          showInput: true,
          confirmButtonLoading: false,
          inputValidator: value => name === value,
          inputErrorMessage: 'Name does not match',
          showCancelButton: true,
          confirmButtonClass: 'el-button--danger',
          confirmButtonText: 'Revoke',
          cancelButtonText: 'Cancel',
          beforeClose: (action, instance, done) => {
            if (action === 'confirm') {
              instance.confirmButtonLoading = true
              instance.confirmButtonText = 'Revoking...'

              const onError = error => {
                console.error(error)

                instance.confirmButtonLoading = false
                instance.confirmButtonText = 'Revoke'

                this.$message.error(error)
              }

              superagent.delete('/api/key/' + name)
                .end((error, result) => {
                  if (result && result.statusCode === 401) return this.logout()
                  if (result && result.statusCode === 409) return onError('Invalid device name')
                  if (result && result.statusCode !== 200) return onError('Something went wrong')
                  if (error) return onError(error)

                  this.refresh()

                  done()
                })
            } else {
              done()
            }
          }
        }).then(action => {}) // handle the promise rejection on dismiss
      },
      prettyDate: (row, column) => {
        const d = new Date(row.created)
        return d.toDateString()
      },
      tableRowClassName ({row, rowIndex}) {
        if (row.connected) return 'connected-row'
        return ''
      },
      toggleRowExpansion (row, event, column) {
        this.$refs.deviceTable.toggleRowExpansion(row)
      }
    }
  })
})()
