#!/usr/bin/env bash

set -eu

# the args to this script are operation(add/update/delete), IP, CN
operation="$1"
ip="$2"
cn="${3:-}" # absent for deletes

curl -sS 'http://127.0.0.1:3000/api/onLearnAddress/' -X POST \
    --data-urlencode "operation=${operation}" \
    --data-urlencode "cn=${cn}" \
    --data-urlencode "vpnIp=${ip}" \
    --data-urlencode "token@/run/admin-token" \
    --max-time 15


