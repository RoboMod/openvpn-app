#!/usr/bin/env bash

source /app/code/easyrsa/easyrsa-vars.sh

echo "Updating CRL"

KEY_ALTNAMES="" KEY_CN="" ${OPENSSL} ca -gencrl -out /app/data/keys/crl.pem -config "$KEY_CONFIG"
