'use strict'

const ldap = require('ldapjs')

const client = ldap.createClient({
  url: process.env.CLOUDRON_LDAP_URL,
  timeout: 10000, /* 10 seconds */
  reconnect: true /* undocumented option to automatically reconnect on connection failure : https://github.com/joyent/node-ldapjs/issues/318#issuecomment-165769581  */
})

const bind = (dn, password) => new Promise((resolve, reject) =>
  client.bind(dn, password, err => err ? reject(err) : resolve())
)

const search = (username) => new Promise((resolve, reject) => {
  const filter = username
    ? {filter: `(|(uid=${username})(mail=${username})(username=${username})(sAMAccountName=${username}))`}
    : {}
  client.search(process.env.CLOUDRON_LDAP_USERS_BASE_DN, filter, (err, res) => {
    if (err) return reject(err)

    const entries = []
    let done = false
    res.on('searchEntry', entry => {
      if (done) return
      entries.push(entry.object)
    })
    res.on('error', err => {
      if (done) return
      done = true
      reject(err)
    })
    res.on('end', result => {
      if (done) return
      done = true
      if (result.status === 0) {
        resolve(entries)
      } else {
        reject(new Error('Unexpected error while retrieving users from LDAP. Status: ' + result.status))
      }
    })
  })
})

const auth = (username, password) => search(username)
  .then(entries => {
    if (entries.length !== 1) {
      throw new Error('Unknown user')
    } else {
      return bind(`cn=${entries[0].username},${process.env.CLOUDRON_LDAP_USERS_BASE_DN}`, password)
        .then(() => ({
          username: entries[0].username,
          displayName: entries[0].displayname,
          email: entries[0].email,
          admin: (entries[0].isadmin.toLowerCase() === 'true' || entries[0].isadmin === '1') // we need to parse string values
        }))
        .catch(() => { throw new Error('Wrong password') })
    }
  })

const cache = {
  data: null,
  duration: /* 1 min: */ 60 /* seconds */ * 1000 /* milliseconds */, // User can change this value to modify duration of caching or disable with 0
  time: Date.now()
}

const listUsers = (useCache = true) => {
  if (useCache && cache.data && Date.now() - cache.time < cache.duration) return Promise.resolve(cache.data)
  return search()
    .then(entries => {
      cache.time = Date.now() // remember the cache
      cache.data = entries
      return entries
    })
}

const doesUserExist = (username, useCache = true) => listUsers(useCache)
  .then(users => users.some(user => user.username === username))

module.exports = {
  auth,
  doesUserExist
}
