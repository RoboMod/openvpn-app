'use strict'

const path = require('path')
const assert = require('assert')
const childProcess = require('child_process')
const fs = require('fs')
const {HttpError} = require('connect-lastmile')
const Archiver = require('archiver')
const ldap = require('./ldap')
const safe = require('safetydance')

const promisify = (func) => (...args) => new Promise((resolve, reject) =>
  func(...args, (err, res) => err ? reject(err) : resolve(res))
)
const exec = promisify(childProcess.exec)
const readdir = promisify(fs.readdir)
const stat = promisify(fs.stat)
const rm = promisify(fs.unlink)
const readFile = promisify(fs.readFile)
const writeFile = promisify(fs.writeFile)
const wait = t => new Promise(resolve => setTimeout(() => resolve(), t))

const RUN_DIR = process.env.CLOUDRON ? '/run/' : path.join(__dirname, '../.dev/run/')
const DATA_DIR = process.env.CLOUDRON ? '/app/data/' : path.join(__dirname, '../.dev/data/')

const ADMIN_TOKEN = fs.readFileSync(path.join(RUN_DIR, 'admin-token'), 'utf8')

// legacy keys had username:devicename. new keys have username/devicename
const CN_REGEXP = /^([A-Za-z0-9.]+)(?:\/|:)([A-Za-z0-9\-_]+)$/

const baseDir = '/app/code/easyrsa'
const keyDir = path.join(DATA_DIR, 'keys')
const hostsDir = path.join(RUN_DIR, '/dnsmasq/hosts')

const env = {
  EASY_RSA: baseDir,
  OPENSSL: 'openssl',
  PKCS11TOOL: 'pkcs11-tool',
  GREP: 'grep',
  KEY_DIR: keyDir,
  PKCS11_MODULE_PATH: 'dummy',
  PKCS11_PIN: 'dummy',
  KEY_SIZE: 2048,
  CA_EXPIRE: 3650,
  KEY_EXPIRE: 3650,
  KEY_COUNTRY: 'US',
  KEY_PROVINCE: 'CA',
  KEY_CITY: 'SanFrancisco',
  KEY_ORG: 'Cloudron',
  KEY_EMAIL: 'support@cloudron.io',
  KEY_OU: 'Cloudron',
  KEY_NAME: 'EasyRSA',
  KEY_CONFIG: '/app/code/easyrsa/openssl-1.0.0.cnf'
}

// Provide either the 4 args, or nothing
const clientConfFile = ({ca, cert, key, tlsAuth} = {}) => `# Client
client
tls-client
dev tun
proto tcp-client
remote ${process.env.CLOUDRON_APP_DOMAIN} ${process.env.VPN_TCP_PORT}
resolv-retry infinite
cipher AES-256-CBC
auth SHA256
script-security 2
keepalive 10 120
remote-cert-tls server

# Keys
${ca
    ? '<ca>\n' + ca + '\n</ca>'
    : 'ca ca.crt'
}
${cert
    ? '<cert>\n' + cert + '\n</cert>'
    : 'cert cert.crt'
}
${key
    ? '<key>\n' + key + '\n</key>'
    : 'key cert.key'
}
${tlsAuth
    ? 'key-direction 1\n<tls-auth>\n' + tlsAuth + '\n</tls-auth>'
    : 'tls-auth ta.key 1'
}

# Security
nobind
persist-key
persist-tun
verb 3
`

const connectedClients = {}
const hostnames = {}

const spawnFile = ({tag, file, args, wantedCode = 0}) => new Promise((resolve, reject) => {
  assert.strictEqual(typeof tag, 'string')
  assert.strictEqual(typeof file, 'string')
  assert(Array.isArray(args))

  console.log(`${tag} spawnFile: ${file} ${args.join(' ')}`)

  const cp = childProcess.spawn(file, args, {env})
  cp.stdout.on('data', data => console.log(`${tag} (stdout): ${data.toString('utf8')}`))

  let errorData = ''
  cp.stderr.on('data', data => {
    errorData += data.toString('utf8') // print only on newline
    while (errorData.includes('\n')) {
      const parts = errorData.split('\n')
      console.log(`${tag} (stderr): ${parts.shift()}`)
      errorData = parts.join('\n')
    }
  })

  cp.on('exit', (code, signal) => {
    if (errorData) console.log(`${tag} (stderr): ${errorData}`)
    if (code || signal) console.log(`${tag} code: ${code}, signal: ${signal}`)
    if (code === wantedCode) return resolve()

    const e = new Error(`${tag} exited with error ${code} signal ${signal}`)
    e.code = code
    e.signal = signal
    reject(e)
  })

  cp.on('error', error => {
    console.log(`${tag} code: ${error.code}, signal: ${error.signal}`)
    reject(error)
  })
})

const cleanUserName = userName => userName.toLowerCase().replace(/[^A-Za-z0-9.]+/g, '')

const cleanDeviceName = deviceName => deviceName.replace(/[^A-Za-z0-9\-_]+/g, '')

const listUserKeys = (username) => readdir(path.join(keyDir, username))
  .then(files => files.filter(file => file.endsWith('.key')))
  .catch(() => []) // dir does not exist until a key is created
  .then(files => Promise.all(files.map(file => stat(path.join(keyDir, username, file))))
    .then(stats => files.map((fileName, i) => {
      const deviceName = fileName.replace(/\.key/, '')
      return {
        name: deviceName,
        hostname: `${deviceName}.${username}`,
        created: stats[i].birthtime,
        connected: (connectedClients[username] && connectedClients[username][deviceName]) || false // object if exists, false if undefined or null
      }
    }))
  )

const list = (req, res, next) => {
  listUserKeys(cleanUserName(req.session.user.username))
    .then(list => res.status(222).send({
      entries: list,
      connectedClients: req.session.user.admin ? connectedClients : null
    }))
    .catch(error => next(new HttpError(500, error)))
}

const createKey = (req, res, next) => {
  const deviceName = decodeURIComponent(req.params[0])

  if (!deviceName || (deviceName !== cleanDeviceName(deviceName))) return next(new HttpError(409, 'Invalid device name'))

  listUserKeys(cleanUserName(req.session.user.username))
    .then(list => {
      if (list.map(e => e.name).includes(deviceName)) return next(new HttpError(409, 'Device already exists'))

      safe.fs.mkdirSync(path.join(keyDir, cleanUserName(req.session.user.username))) // ensure directory

      return spawnFile({
        tag: 'createUserKey',
        file: path.join(baseDir, 'pkitool'),
        args: [ `${cleanUserName(req.session.user.username)}/${deviceName}` ]
      })
        .then(() => res.status(201).send({created: deviceName}))
    })
    .catch(error => next(new HttpError(500, error)))
}

const getKey = (req, res, next) => {
  const deviceName = decodeURIComponent(req.params[0])
  const format = req.query['format'] || 'conf'
  const zip = !(req.query['zip'] === 'false') // default to true

  if (!deviceName || (deviceName !== cleanDeviceName(deviceName))) return next(new HttpError(409, 'Invalid device name'))

  let internalPathPrefix // path inside the zip file, if zipped
  let configExt // extension of the config file

  if (format === 'conf') {
    internalPathPrefix = ''
    configExt = 'conf'
  } else if (format === 'ovpn') {
    internalPathPrefix = ''
    configExt = 'ovpn'
  } else if (format === 'tblk') {
    if (!zip) return next(new HttpError(409, 'Invalid format: cannot disable zip for tblk'))
    internalPathPrefix = `${process.env.CLOUDRON_APP_DOMAIN}-${deviceName}.tblk/Contents/Resources/`
    configExt = 'ovpn'
  } else {
    return next(new HttpError(409, 'Invalid format'))
  }

  listUserKeys(cleanUserName(req.session.user.username))
    .then(list => {
      if (!list.map(e => e.name).includes(deviceName)) return next(new HttpError(404, 'Not Found'))
      const certFile = `${cleanUserName(req.session.user.username)}/${deviceName}.crt`
      const keyFile = `${cleanUserName(req.session.user.username)}/${deviceName}.key`

      if (zip) {
        res.header('Content-Type', 'application/zip')
        res.header('Content-Disposition', `attachment; filename="${process.env.CLOUDRON_APP_DOMAIN}-${deviceName}-${format}.zip"`)

        const archive = new Archiver('zip')
        archive.on('warning', (err) => console.error('ZIP WARNING:', err))
        archive.on('error', (err) => {
          console.error('ZIP ERROR:', err)
          res.end()
        })
        archive.pipe(res)

        archive.file(path.join(keyDir, 'ca.crt'), {name: internalPathPrefix + 'ca.crt'})
        archive.file(path.join(keyDir, certFile), {name: internalPathPrefix + 'cert.crt'})
        archive.file(path.join(keyDir, keyFile), {name: internalPathPrefix + 'cert.key'})
        archive.file(path.join(keyDir, 'ta.key'), {name: internalPathPrefix + 'ta.key'})
        archive.append(
          clientConfFile(),
          {name: internalPathPrefix + 'config.' + configExt}
        )
        archive.finalize()
      } else {
        return Promise.all([
          readFile(path.join(keyDir, 'ca.crt'), 'utf8'),
          readFile(path.join(keyDir, certFile), 'utf8'),
          readFile(path.join(keyDir, keyFile), 'utf8'),
          readFile(path.join(keyDir, 'ta.key'), 'utf8')
        ])
          .then(([ca, cert, key, tlsAuth]) => {
            res.header('Content-Type', 'application/data')
            res.header('Content-Disposition', `attachment; filename="${process.env.CLOUDRON_APP_DOMAIN}-${deviceName}.${configExt}"`)
            res.send(clientConfFile({
              ca,
              cert,
              key,
              tlsAuth
            }))
          })
      }
    })
    .catch(error => next(new HttpError(500, error)))
}

const revokeKey = (req, res, next) => {
  const deviceName = decodeURIComponent(req.params[0])

  if (!deviceName || (deviceName !== cleanDeviceName(deviceName))) return next(new HttpError(409, 'Invalid device name'))

  listUserKeys(cleanUserName(req.session.user.username))
    .then(list => {
      if (!list.map(e => e.name).includes(deviceName)) return next(new HttpError(404, 'Not Found'))

      return spawnFile({
        tag: 'revokeUserKey',
        file: path.join(baseDir, 'revoke-full'),
        args: [ `${cleanUserName(req.session.user.username)}/${deviceName}` ],
        wantedCode: 2
      })
        .then(() => rm(path.join(keyDir, `${cleanUserName(req.session.user.username)}/${deviceName}.key`)))
        .then(() => res.status(200).send({revoked: deviceName}))
    })
    .catch(error => next(new HttpError(500, error)))
}

const onClientConnect = (req, res, next) => {
  const cn = req.body['cn']
  const remoteIp = req.body['ip']
  const vpnIp = req.body['vpnIp']
  const token = req.body['token']

  if (token !== ADMIN_TOKEN) return next(new HttpError(401, 'Unauthorized'))
  if (!cn || !remoteIp || !vpnIp) return next(new HttpError(400, 'Invalid Request'))

  const match = CN_REGEXP.exec(cn) // cn is the full path of the key that matched
  if (!match) return next(new HttpError(400, `Invalid CN: ${cn}`))
  const [, user, deviceName] = match

  Promise.race([
    ldap.doesUserExist(user),
    wait(10000).then(() => { throw new Error('LDAP request timeout') })
  ])
    .then(exists => {
      if (!exists) return next(new HttpError(403, 'Unauthorized'))
      if (!connectedClients[user]) connectedClients[user] = {}

      connectedClients[user][deviceName] = {remoteIp, vpnIp}
      return res.status(200).send({connected: {user, deviceName, remoteIp, vpnIp}})
    })
    .catch(error => next(new HttpError(500, error)))
}

const onClientDisconnect = (req, res, next) => {
  const cn = req.body['cn']
  const token = req.body['token']

  if (token !== ADMIN_TOKEN) return next(new HttpError(401, 'Unauthorized'))
  if (!cn) return next(new HttpError(400, 'Invalid Request'))

  const match = CN_REGEXP.exec(cn) // cn is the full path of the key that matched
  if (!match) return next(new HttpError(400, `Invalid CN: ${cn}`))
  const [, user, deviceName] = match

  if (connectedClients[user] && connectedClients[user][deviceName]) {
    delete connectedClients[user][deviceName]
  }

  return res.status(200).send({disconnected: {user, deviceName}})
}

const onLearnAddress = (req, res, next) => {
  const operation = req.body['operation']
  const vpnIp = req.body['vpnIp']
  const cn = req.body['cn'] // won't be set for delete
  const token = req.body['token']

  if (token !== ADMIN_TOKEN) return next(new HttpError(401, 'Unauthorized'))
  if (!operation || !vpnIp) return next(new HttpError(400, 'Invalid Request'))
  if (!operation.match(/^(add|update|delete)$/)) return next(new HttpError(400, 'Invalid operation'))
  if (operation.match(/^(add|update)$/) && !cn) return next(new HttpError(400, 'cn is required'))

  if (operation === 'add' || operation === 'update') {
    const match = CN_REGEXP.exec(cn) // cn is the full path of the key that matched
    if (!match) return next(new HttpError(400, `Invalid CN: ${cn}`))
    const [, user, deviceName] = match

    const hostname = deviceName + '.' + user

    hostnames[vpnIp] = `${hostname} ${hostname}.${process.env.CLOUDRON_APP_DOMAIN}`
  } else if (operation === 'delete') {
    delete hostnames[vpnIp]
  }

  const hostsFile = Object.keys(hostnames).map((ip) => `${ip} ${hostnames[ip]}`).join('\n') + '\n'
  writeFile(path.join(hostsDir, 'cloudron_hosts'), hostsFile, 'utf8')
    .then(() => res.status(200).send({learned: {operation, vpnIp}}))
    .catch(error => next(new HttpError(500, error)))
}

const isRunning = () => exec('supervisorctl status openvpn')
  .then(res => res
    .toString()
    .includes('RUNNING')
  )
  .catch(() => false)

module.exports = {
  list,
  createKey,
  getKey,
  revokeKey,
  isRunning,
  onClientConnect,
  onClientDisconnect,
  onLearnAddress
}
