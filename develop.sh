#!/bin/bash

set -eu

echo "Ensure to set LDAP_URL"

# ensure local app data
mkdir -p .dev/run/ .dev/data/ .dev/run/dnsmasq/hosts/ .dev/data/keys/

# Creating a secret for web sessions
if [ ! -f .dev/data/session.secret ]; then
    dd if=/dev/urandom bs=256 count=1 | base64 > .dev/data/session.secret
fi

# Generate random management token for admin api
dd if=/dev/urandom bs=256 count=1 | base64 > .dev/run/admin-token

echo "Starting server"
exec ./server.js
