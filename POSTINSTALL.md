This app allows Cloudron users to login to the VPN app and generate a certificate on their
own for each device they own.

Admins can customize the VPN configuration by editing `/app/data/openvpn.conf` using the
Web Terminal. Please see the [docs](https://cloudron.io/documentation/apps/openvpn/) for
more information.

