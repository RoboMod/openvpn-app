#!/usr/bin/env node
'use strict'

const express = require('express')
const morgan = require('morgan')
const path = require('path')
const compression = require('compression')
const session = require('express-session')
const lastMile = require('connect-lastmile')
const {HttpSuccess, HttpError} = require('connect-lastmile')
const ldap = require('./src/ldap')
const bodyParser = require('body-parser')
const fs = require('fs')
const openvpn = require('./src/openvpn')
const LokiStore = require('connect-loki')(session)

const app = express()
const router = new express.Router()

const urlEncodedParser = bodyParser.urlencoded({extended: true})
const jsonParser = bodyParser.json({strict: true})

const baseDir = process.env.CLOUDRON ? '/app/data' : path.join(__dirname, '.dev/data')

const isAuthenticated = (req, res, next) => (req.session && req.session.user) ? next() : res.status(401).send({})

app.use('/api/healthcheck', (req, res) => openvpn.isRunning()
  .then(running => running ? res.status(200).send() : res.status(500).send())
)

app.set('trust proxy', 1)
app.use(morgan('dev'))
app.use(compression())
app.use(session({
  secret: fs.readFileSync(path.resolve(__dirname, `${baseDir}/session.secret`), 'utf8'),
  store: new LokiStore({
    path: path.resolve(__dirname, `${baseDir}/session.db`),
    logErrors: true
  }),
  resave: false,
  saveUninitialized: false,
  name: 'openvpn.sid',
  cookie: {
    path: '/',
    httpOnly: true,
    secure: !!process.env.CLOUDRON,
    maxAge: /* 1 week: */ 7 /* d */ * 24 /* h */ * 60 /* min */ * 60 /* s */ * 1000 /* ms */
  }
}))
router.post('/api/login', jsonParser, (req, res, next) => {
  if (!req.body.username || !req.body.password) {
    req.session.user = null
    next(new HttpError(401, 'Unauthorized'))
  } else {
    ldap.auth(req.body.username, req.body.password)
      .then(profile => {
        req.session.user = profile

        next(new HttpSuccess(200, {user: req.session.user}))
      })
      .catch(() => next(new HttpError(401, 'Unauthorized')))
  }
})
router.get('/api/logout', (req, res) => {
  req.session.user = null
  res.redirect('/')
})
router.get('/api/profile', isAuthenticated, (req, res, next) => {
  next(new HttpSuccess(200, {user: req.session.user}))
})
router.post('/api/onConnect/', urlEncodedParser, openvpn.onClientConnect)
router.post('/api/onDisconnect/', urlEncodedParser, openvpn.onClientDisconnect)
router.post('/api/onLearnAddress/', urlEncodedParser, openvpn.onLearnAddress)
router.get('/api/list/', isAuthenticated, openvpn.list)
router.put('/api/key/*', isAuthenticated, openvpn.createKey)
router.get('/api/key/*', isAuthenticated, openvpn.getKey)
router.delete('/api/key/*', isAuthenticated, openvpn.revokeKey)
app.use(router)
app.use('/', express.static(path.resolve(__dirname, 'frontend')))
app.use(lastMile())

const server = app.listen(3000, '0.0.0.0', () => {
  const {address, port} = server.address()

  console.log(`OpenVPN web-config interface listening at http://${address}:${port}`)
})
